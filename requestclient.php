<?php
	require_once("lib/nusoap.php");
	$wsdl = "http://localhost/soap/server.php?wsdl";
	$client = new nusoap_client($wsdl, 'wsdl');
	$err = $client->getError();
	if ($err) 
	{
		echo '<h2>Constructor error</h2>' . $err;
	    exit();
	}
	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$mid = test_input($_POST["mid"]);
		$pid = test_input($_POST["pid"]);
		$keyword = test_input($_POST["keyword"]);
	}
	if(!empty($_POST["submit"]))
	{
		$param = array('msid' => $mid,'pid' => $pid, 'keyword' => $keyword);
		$result1=$client->call('request', $param);
	}
	
	//print_r($result1); 
	function test_input($data) 
  	{
	    $data = trim($data);
	    $data = stripslashes($data);
	    $data = htmlspecialchars($data);
	    return $data;
  	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Service Report</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<!-- for search -->
	<div class="container">
		<div class="row">
			<div class="col-md-6">
		    		<h2>Request Report</h2>
		    </div>
		</div><br/>
	    <div class="row">
	        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>">
	        	<div class="col-md-2">
					<div class="form-group">
						<input type="text" name="mid" class="form-control" placeholder="Msisdn">
					</div>
	        	</div>
	        	<div class="col-md-2">
					<div class="form-group">
						<input type="text" name="pid" class="form-control" placeholder="Plan ID">
					</div>
	        	</div>
	        	<div class="col-md-2">
					<div class="form-group">
						<input type="text" name="keyword" class="form-control" placeholder="Keyword">
					</div>
	        	</div>
	        	<div class="col-md-1">
					<input type="submit" name="submit" class="btn btn-primary" value="Search">
	        	</div>
	        </form>
	    </div>
	    <?php
		if(empty($mid) && empty($pid) && empty($keyword))
		{
			echo "
			<div class='well well-sm'>
				<h3 style='color:blue;'>Type To Search!</h3>
	      			<span style='color:blue'><i>Hints:</i></span><br/>
	      			<ul>
						<li><em>For better result Short Code,Service Type,Plan ID should be accurate.</em></li>
					</ul> 
	      		</div>";
		}
		else
		{
		?>
			<div class='panel panel-default'>
			<div class='panel-body'>
				<table class='table table-striped' style='word-wrap:break-word;'>
			    <thead>
				    <tr>
				       	<th>ID</th>
				       	<th>Msisdn</th>
				       	<th>Keyword</th>
				       	<th>Plan ID</th>
				       	<th>Request Type</th>
				    </tr>
			    </thead>
				<tbody>
					<tr>
						<td><?php echo $result1['uid']; ?></td>
						<td><?php echo $result1['msid'];?></td>
						<td><?php echo $result1['keywords'];?></td>
						<td><?php echo $result1['plnid'];?></td>
						<td><?php echo $result1['rtyp'];?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<?php
	}
	?>
</body>
</html>