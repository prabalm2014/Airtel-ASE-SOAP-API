<?php
	require_once("lib/nusoap.php");
	$wsdl = "http://localhost/soap/server.php?wsdl";
	$client = new nusoap_client($wsdl, 'wsdl');
	$err = $client->getError();
	if ($err) 
	{
		echo '<h2>Constructor error</h2>' . $err;
	    exit();
	}
	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$keyword = test_input($_POST["keyword"]);
		$pid = test_input($_POST["pid"]);
		$sid = test_input($_POST["sid"]);
	}
	if(!empty($_POST["submit"]))
	{
		$param = array('keyword' => $keyword,'pid' => $pid, 'sid' => $sid);
		$result1=$client->call('serviceList', $param);
	}
	
	//print_r($result1); 
	function test_input($data) 
  	{
	    $data = trim($data);
	    $data = stripslashes($data);
	    $data = htmlspecialchars($data);
	    return $data;
  	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Service Report</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<!-- for search -->
	<div class="container">
		<div class="row">
			<div class="col-md-6">
		    		<h2>Service Report</h2>
		        </div>
		</div><br/>
	    <div class="row">
	        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>">
	        	<div class="col-md-2">
					<div class="form-group">
						<input type="text" name="keyword" class="form-control" placeholder="Keyword">
					</div>
	        	</div>
	        	<!--<div class="col-md-2">
					<div class="form-group">
						<input type="text" name="shcode" class="form-control" required placeholder="Short Code">
					</div>
	        	</div>
	        	<div class="col-md-2">
					<div class="form-group">
						<select name="type" class="form-control">
							<option value="" disabled="disabled" selected>Service Type</option>
							<option value="call">CALL</option>
							<option value="sms">SMS</option>
							<option value="voice sms">VOICE SMS</option>
						</select>
					</div>
	        	</div>-->
	        	<div class="col-md-2">
					<div class="form-group">
						<input type="text" name="pid" class="form-control" placeholder="Plan ID">
					</div>
	        	</div>
	        	<div class="col-md-2">
					<div class="form-group">
						<input type="text" name="sid" class="form-control" placeholder="Service ID">
					</div>
	        	</div>
	        	<div class="col-md-1">
					<input type="submit" name="submit" class="btn btn-primary" value="Search">
	        	</div>
	        </form>
	    </div>
	    <?php
		if(empty($keyword) && empty($shcode) && empty($type) && empty($pid) && empty($sid))
		{
			echo "
			<div class='well well-sm'>
				<h3 style='color:blue;'>Type To Search!</h3>
	      			<span style='color:blue'><i>Hints:</i></span><br/>
	      			<ul>
						<li><em>For better result Short Code,Service Type,Plan ID should be accurate.</em></li>
					</ul> 
	      		</div>";
		}
		else
		{
		?>
			<div class='panel panel-default'>
			<div class='panel-body'>
				<table class='table table-striped' style='word-wrap:break-word;'>
			    <thead>
				    <tr>
				       	<th>ID</th>
				       	<th>Service Name</th>
				       	<th>Keyword</th>
				       	<th>Base Price</th>
				       	<th>Short Code</th>
				       	<th>Service Node</th>
				       	<th>Plan ID</th>
				       	<th>Service Type</th>
				       	<th>Active Day</th>
				       	<th>Service ID</th>
				    </tr>
			    </thead>
				<tbody>
					<tr>
						<td><?php echo $result1['uid']; ?></td>
						<td><?php echo $result1['servicename'];?></td>
						<td><?php echo $result1['keywords'];?></td>
						<td><?php echo $result1['baseprice'];?></td>
						<td><?php echo $result1['shortcodes'];?></td>
						<td><?php echo $result1['servicenode'];?></td>
						<td><?php echo $result1['planid'];?></td>
						<td><?php echo $result1['servicetype'];?></td>
						<td><?php echo $result1['activedays'];?></td>
						<td><?php echo $result1['serviceid'];?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<?php
	}
	?>
</body>
</html>