<?php
	require_once 'lib/nusoap.php';
	//error_reporting(0);

	$server = new nusoap_server;
	$server->configureWSDL('server', 'urn:server');
	$server->wsdl->schemaTargetNamespace = 'urn:server';
	$server->wsdl->addComplexType(
	    'Person',
	    'complexType',
	    'struct',
	    'all',
	    '',
	    array(
	        'uid' => array('name' => 'uid', 'type' => 'xsd:int'),
	        'servicename' => array('name' => 'servicename', 'type' => 'xsd:string'),
	        'keywords' => array('name' => 'keywords', 'type' => 'xsd:string'),
	        'baseprice' => array('name' => 'baseprice', 'type' => 'xsd:string'),
	        'shortcodes' => array('name' => 'shortcodes', 'type' => 'xsd:string'),
	        'servicenode' => array('name' => 'servicenode', 'type' => 'xsd:string'),
	        'planid' => array('name' => 'planid', 'type' => 'xsd:string'),
	        'servicetype' => array('name' => 'servicetype', 'type' => 'xsd:string'),
	        'activedays' => array('name' => 'activedays', 'type' => 'xsd:string'),
	        'serviceid' => array('name' => 'serviceid', 'type' => 'xsd:string')
	    )
	);

	$server->wsdl->addComplexType(
	    'Subs',
	    'complexType',
	    'struct',
	    'all',
	    '',
	    array(
	    	'uid' => array('name' => 'uid', 'type' => 'xsd:int'),
	    	'msisdns' => array('name' => 'msisdns', 'type' => 'xsd:string'),
	    	'planid' => array('name' => 'planid', 'type' => 'xsd:string'),
	    	'keywords' => array('name' => 'keywords', 'type' => 'xsd:string'),
	    	'shortcodes' => array('name' => 'shortcodes', 'type' => 'xsd:string'),
	        'initialstatus' => array('name' => 'initialstatus', 'type' => 'xsd:string'),
	        'previousstatus' => array('name' => 'previousstatus', 'type' => 'xsd:string'),
	        'currentstatus' => array('name' => 'currentstatus', 'type' => 'xsd:string'),
	        'servicetype' => array('name' => 'servicetype', 'type' => 'xsd:string')
	    )
	);

	$server->wsdl->addComplexType(
	    'Call',
	    'complexType',
	    'struct',
	    'all',
	    '',
	    array(
	        'uid' => array('name' => 'uid', 'type' => 'xsd:int'),
	        'msdn' => array('name' => 'msdn', 'type' => 'xsd:string'),
	        'srvid' => array('name' => 'srvid', 'type' => 'xsd:string'),
	        'stype' => array('name' => 'stype', 'type' => 'xsd:string'),
	        'plnid' => array('name' => 'plnid', 'type' => 'xsd:string'),
	        'errcod' => array('name' => 'errcod', 'type' => 'xsd:string'),
	        'operation' => array('name' => 'operation', 'type' => 'xsd:string'),
	        'result' => array('name' => 'result', 'type' => 'xsd:string'),
	        'transid' => array('name' => 'transid', 'type' => 'xsd:string'),
	        'contid' => array('name' => 'contid', 'type' => 'xsd:string'),
	        'catagry' => array('name' => 'catagry', 'type' => 'xsd:string'),
	        'chrgamont' => array('name' => 'chrgamont', 'type' => 'xsd:string'),
	        'applidpln' => array('name' => 'applidpln', 'type' => 'xsd:string'),
	        'enddts' => array('name' => 'enddts', 'type' => 'xsd:string'),
	        'valdate' => array('name' => 'valdate', 'type' => 'xsd:string')
	    )
	);
	
	$server->wsdl->addComplexType(
	    'Request',
	    'complexType',
	    'struct',
	    'all',
	    '',
	    array(
	        'uid' => array('name' => 'uid', 'type' => 'xsd:int'),
	        'msid' => array('name' => 'msid', 'type' => 'xsd:string'),
	        'keywords' => array('name' => 'keywords', 'type' => 'xsd:string'),
	        'plnid' => array('name' => 'plnid', 'type' => 'xsd:string'),
	        'rtyp' => array('name' => 'rtyp', 'type' => 'xsd:string')
	    )
	);

	//-----------------------------------------------------------------------------------------------------


	$server->register('serviceList',
	array('keyword'=>'xsd:string', 'pid'=>'xsd:string', 'sid'=>'xsd:string'),
	//array('username' => 'xsd:string', 'password'=>'xsd:string'),  //parameters
	array('return' => 'tns:Person'),  //output
	'urn:server',   //namespace
	'urn:server#servicelistServer',  //soapaction
	'rpc', // style
	'encoded', // use
	'Just say hello');  //description

	$server->register('subscriber',
		array('msisdn'=>'xsd:string', 'pid'=>'xsd:string', 'keyword'=>'xsd:string'),
		array('return' => 'tns:Subs'),  //output
		'urn:server',   //namespace
		'urn:server#subscriberlistServer',  //soapaction
		'rpc', // style
		'encoded', // use
		'subscriber');  //description

	$server->register('callback',
		array('msisdn'=>'xsd:string', 'sid'=>'xsd:string', 'pid'=>'xsd:string'),
		array('return' => 'tns:Call'),  //output
		'urn:server',   //namespace
		'urn:server#CallbackServer',  //soapaction
		'rpc', // style
		'encoded', // use
		'callback');  //description

	$server->register('request',
		array('msid'=>'xsd:string', 'pid'=>'xsd:string', 'keyword'=>'xsd:string'),
		array('return' => 'tns:Request'),  //output
		'urn:server',   //namespace
		'urn:server#subscriberlistServer',  //soapaction
		'rpc', // style
		'encoded', // use
		'requested');  //description


	function serviceList($keyword,$pid,$sid)
	{
		$servername = "localhost";
		$username = "root";
		$password = "";
		$db = "airtel_ase";

		$conn = mysqli_connect($servername, $username, $password,$db);

		if (!$conn) {
	    die("Connection failed: " . mysqli_connect_error());
		}
		
		$sql = "SELECT * FROM service_list where keyword like '%$keyword%' or plan_id like '%$pid%' or service_id like '%$sid%'";
		$result = $conn->query($sql);
		$row = $result->fetch_assoc();

		return array(
			'uid' => $row['id'],
		    'servicename' => $row['service_name'],
		    'keywords' => $row['keyword'],
	        'baseprice' => $row['base_price'],
		    'shortcodes' => $row['shortcode'],
		    'servicenode' => $row['service_node'],
		    'planid' => $row['plan_id'],
		    'servicetype' => $row['service_type'],
		    'activedays' => $row['active_days'],
		    'serviceid' => $row['service_id']
		);
	}

	function subscriber($msisdn,$pid,$keyword)
	{
		$servername = "localhost";
		$username = "root";
		$password = "";
		$db = "airtel_ase";

		$conn = mysqli_connect($servername, $username, $password,$db);

		if (!$conn) {
	    die("Connection failed: " . mysqli_connect_error());
		}
		$sql = "SELECT * FROM subscribers where msisdn like '%$msisdn%' or plan_id like '%$pid%' or keyword like '%$keyword%' ";
		$result = $conn->query($sql);
		$row = $result->fetch_assoc();
		return array(
			'uid' => $row['id'],
		    'msisdns' => $row['msisdn'],
		    'planid' => $row['plan_id'],
	        'keywords' => $row['keyword'],
		    'shortcodes' => $row['shortcode'],
		    'initialstatus' => $row['initial_status'],
		    'previousstatus' => $row['previous_status'],
		    'currentstatus' => $row['current_status'],
		    'servicetype' => $row['service_type']
		);
	}

	function callback($msisdn,$sid,$pid)
	{
		$servername = "localhost";
		$username = "root";
		$password = "";
		$db = "airtel_ase";

		$conn = mysqli_connect($servername, $username, $password,$db);

		if (!$conn) {
	    die("Connection failed: " . mysqli_connect_error());
		}
		$sql = "SELECT * FROM callback_logs where msisdn like '%$msisdn%' or serviceId like '%$sid%' and planId like '%$pid%'";
		$result = $conn->query($sql);
		$row = $result->fetch_assoc();
		return array(
			'uid' => $row['id'],
			'msdn' => $row['msisdn'],
			'srvid' => $row['serviceId'],
			'stype' => $row['serviceType'],
			'plnid' => $row['planId'],
			'errcod' => $row['errorCode'],
			'operation' => $row['operation'],
			'result' => $row['result'],
			'transid' => $row['transId'],
			'contid' => $row['contentId'],
			'catagry' => $row['category'],
			'chrgamont' => $row['chargeAmount'],
			'applidpln' => $row['appliedPlan'],
			'enddts' => $row['endDate'],
			'valdate' => $row['validityDays']
		);
	}

	function request($msid,$pid,$keyword)
	{
		$servername = "localhost";
		$username = "root";
		$password = "";
		$db = "airtel_ase";

		$conn = mysqli_connect($servername, $username, $password,$db);

		if (!$conn) {
	    die("Connection failed: " . mysqli_connect_error());
		}
		
		$sql = "SELECT * FROM request_logs where msisdn like '%$msid%' or plan_id like '%$pid%' or keyword like '%$keyword%' ";
		$result = $conn->query($sql);
		$row = $result->fetch_assoc();

		return array(
			'uid' => $row['id'],
		    'msid' => $row['msisdn'],
		    'keywords' => $row['keyword'],
	        'plnid' => $row['plan_id'],
		    'rtyp' => $row['request_type']
		);
	}

	$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
	$server->service($HTTP_RAW_POST_DATA);
?>