-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 12, 2017 at 06:14 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `airtel_ase`
--

-- --------------------------------------------------------

--
-- Table structure for table `ase_status`
--

CREATE TABLE `ase_status` (
  `id` int(11) NOT NULL,
  `status_code_ase` varchar(100) DEFAULT NULL,
  `status_code_ssl` varchar(100) DEFAULT NULL,
  `detaiuls` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ase_status`
--

INSERT INTO `ase_status` (`id`, `status_code_ase`, `status_code_ssl`, `detaiuls`) VALUES
(1, '2828', '4000', 'internet offer');

-- --------------------------------------------------------

--
-- Table structure for table `callback_logs`
--

CREATE TABLE `callback_logs` (
  `id` int(11) NOT NULL,
  `msisdn` varchar(100) DEFAULT NULL,
  `serviceId` varchar(100) DEFAULT NULL,
  `serviceType` varchar(100) DEFAULT NULL,
  `planId` varchar(100) DEFAULT NULL,
  `errorCode` varchar(100) DEFAULT NULL,
  `operation` varchar(100) DEFAULT NULL,
  `result` varchar(100) DEFAULT NULL,
  `transId` varchar(100) DEFAULT NULL,
  `contentId` varchar(100) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `chargeAmount` varchar(100) DEFAULT NULL,
  `appliedPlan` varchar(100) DEFAULT NULL,
  `endDate` varchar(100) DEFAULT NULL,
  `validityDays` varchar(100) DEFAULT NULL,
  `remarks` text,
  `incomming_time` datetime DEFAULT NULL,
  `raw_data` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `callback_logs`
--

INSERT INTO `callback_logs` (`id`, `msisdn`, `serviceId`, `serviceType`, `planId`, `errorCode`, `operation`, `result`, `transId`, `contentId`, `category`, `chargeAmount`, `appliedPlan`, `endDate`, `validityDays`, `remarks`, `incomming_time`, `raw_data`) VALUES
(1, 'M10', 'S10', 'call', 'P10', '404', 'call', 'good', 'T10', 'C10', 'call', '30', 'bondhu', '30/10/16', '20/11/16', 'good', '2016-09-15 00:00:00', 'dkvd dvdv dkvd'),
(2, 'M20', 'S20', 'sms', 'P10', '2929', 'sms', 'good', 'T20', 'c20', 'sms', '40', 'bondhu', '20/08/16', '7', 'gffg  fgf', '2016-09-26 00:00:00', 'hghg');

-- --------------------------------------------------------

--
-- Table structure for table `callback_logs_copy`
--

CREATE TABLE `callback_logs_copy` (
  `id` int(11) NOT NULL,
  `msisdn` varchar(100) DEFAULT NULL,
  `serviceId` varchar(100) DEFAULT NULL,
  `serviceType` varchar(100) DEFAULT NULL,
  `planId` varchar(100) DEFAULT NULL,
  `errorCode` varchar(100) DEFAULT NULL,
  `operation` varchar(100) DEFAULT NULL,
  `result` varchar(100) DEFAULT NULL,
  `transId` varchar(100) DEFAULT NULL,
  `contentId` varchar(100) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `chargeAmount` varchar(100) DEFAULT NULL,
  `appliedPlan` varchar(100) DEFAULT NULL,
  `endDate` varchar(100) DEFAULT NULL,
  `validityDays` varchar(100) DEFAULT NULL,
  `remarks` text,
  `incomming_time` datetime DEFAULT NULL,
  `raw_data` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `request_logs`
--

CREATE TABLE `request_logs` (
  `id` int(11) NOT NULL,
  `msisdn` varchar(100) DEFAULT NULL,
  `keyword` varchar(100) DEFAULT NULL,
  `plan_id` varchar(100) DEFAULT NULL,
  `request_type` varchar(100) DEFAULT NULL,
  `request_data` text,
  `response_data` text,
  `hittime` datetime DEFAULT NULL,
  `response_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request_logs`
--

INSERT INTO `request_logs` (`id`, `msisdn`, `keyword`, `plan_id`, `request_type`, `request_data`, `response_data`, `hittime`, `response_time`) VALUES
(1, 'M10', 'FNF', 'P10', 'sms', 'fnf list', '0177777777777\r\n0199999999999\r\n0166666666666', '2016-09-15 00:00:00', '2016-09-16 00:01:12'),
(2, 'M20', 'Eid Offer', 'P20', 'sms', 'eid offer', '250 MB 30 TK', '2016-09-16 07:30:00', '2016-09-15 07:30:30');

-- --------------------------------------------------------

--
-- Table structure for table `service_list`
--

CREATE TABLE `service_list` (
  `id` int(11) NOT NULL,
  `service_name` varchar(100) DEFAULT NULL,
  `keyword` varchar(100) DEFAULT NULL,
  `base_price` varchar(100) DEFAULT NULL,
  `shortcode` varchar(100) DEFAULT NULL,
  `service_node` varchar(100) DEFAULT NULL,
  `plan_id` varchar(100) DEFAULT NULL,
  `service_type` varchar(100) DEFAULT NULL,
  `plan_desc` varbinary(200) DEFAULT NULL,
  `active_days` varchar(100) DEFAULT NULL,
  `is_active` int(1) DEFAULT '1',
  `service_id` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_list`
--

INSERT INTO `service_list` (`id`, `service_name`, `keyword`, `base_price`, `shortcode`, `service_node`, `plan_id`, `service_type`, `plan_desc`, `active_days`, `is_active`, `service_id`) VALUES
(1, 'sms', 'Eid Offer', '2', '2800', '30', 'P10', 'sms', 0x456964204f66666572, '7', 1, 'S10'),
(2, 'call', 'gp info', '2', '2800', '30', 'P10', 'call', 0x63616c6c, '7', 1, 'S10'),
(3, 'call', 'Eid Offer', '2', '2828', '30', 'P10', 'call', 0x456964204f66666572, '7', 1, 'S20');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(11) NOT NULL,
  `msisdn` varchar(100) DEFAULT NULL,
  `plan_id` varchar(100) DEFAULT NULL,
  `keyword` varchar(100) DEFAULT NULL,
  `shortcode` varchar(100) DEFAULT NULL,
  `initial_status` varchar(100) DEFAULT NULL,
  `previous_status` varchar(100) DEFAULT NULL,
  `current_status` varchar(100) DEFAULT NULL,
  `final_status` varchar(100) NOT NULL DEFAULT 'subscribe',
  `incomming_time` datetime DEFAULT NULL,
  `last_update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status_update_time` datetime DEFAULT NULL,
  `service_type` varchar(100) DEFAULT NULL,
  `remarks` varchar(200) DEFAULT NULL,
  `count` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `msisdn`, `plan_id`, `keyword`, `shortcode`, `initial_status`, `previous_status`, `current_status`, `final_status`, `incomming_time`, `last_update_time`, `status_update_time`, `service_type`, `remarks`, `count`) VALUES
(1, 'M10', 'P10', 'Eid offer', '02000', 'requested', 'inactive', 'active', 'subscribe', '2016-09-17 00:00:00', '2016-09-16 18:00:00', '2016-09-17 00:00:00', 'sms', 'sms', 1),
(2, 'M20', 'P20', 'gp info', '2800', 'request', 'inactive', 'active', 'subscribe', '2016-09-23 00:00:00', '2016-09-23 18:00:00', '2016-09-25 00:00:00', 'sms', 'sms', 2);

-- --------------------------------------------------------

--
-- Table structure for table `subscribers_copy`
--

CREATE TABLE `subscribers_copy` (
  `id` int(11) NOT NULL,
  `msisdn` varchar(100) DEFAULT NULL,
  `plan_id` varchar(100) DEFAULT NULL,
  `keyword` varchar(100) DEFAULT NULL,
  `shortcode` varchar(100) DEFAULT NULL,
  `initial_status` varchar(100) DEFAULT NULL,
  `previous_status` varchar(100) DEFAULT NULL,
  `current_status` varchar(100) DEFAULT NULL,
  `final_status` varchar(100) NOT NULL DEFAULT 'subscribe',
  `incomming_time` datetime DEFAULT NULL,
  `last_update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status_update_time` datetime DEFAULT NULL,
  `service_type` varchar(100) DEFAULT NULL,
  `remarks` varchar(200) DEFAULT NULL,
  `count` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `fullname` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `type` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `fullname`, `email`, `type`, `password`) VALUES
(1, 'prabal', 'pm@gmail.com', 'admin', 'e10adc3949ba59abbe56e057f20f883e'),
(2, 'tanvir', 'tanvir@gmail.com', 'reporter', 'e10adc3949ba59abbe56e057f20f883e'),
(3, 'costa', 'costa@gmail.com', 'support', 'e10adc3949ba59abbe56e057f20f883e');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ase_status`
--
ALTER TABLE `ase_status`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`status_code_ase`);

--
-- Indexes for table `callback_logs`
--
ALTER TABLE `callback_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `msisdn` (`msisdn`);

--
-- Indexes for table `callback_logs_copy`
--
ALTER TABLE `callback_logs_copy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_logs`
--
ALTER TABLE `request_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `msisdn` (`msisdn`);

--
-- Indexes for table `service_list`
--
ALTER TABLE `service_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `msisdn` (`msisdn`);

--
-- Indexes for table `subscribers_copy`
--
ALTER TABLE `subscribers_copy`
  ADD PRIMARY KEY (`id`),
  ADD KEY `msisdn` (`msisdn`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ase_status`
--
ALTER TABLE `ase_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `callback_logs`
--
ALTER TABLE `callback_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `callback_logs_copy`
--
ALTER TABLE `callback_logs_copy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `request_logs`
--
ALTER TABLE `request_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `service_list`
--
ALTER TABLE `service_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `subscribers_copy`
--
ALTER TABLE `subscribers_copy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
