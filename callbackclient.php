<?php
	require_once("lib/nusoap.php");
	$wsdl = "http://localhost/soap/server.php?wsdl";
	$client = new nusoap_client($wsdl, 'wsdl');
	$err = $client->getError();
	if ($err) 
	{
		echo '<h2>Constructor error</h2>' . $err;
	    exit();
	}
	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$msisdn = test_input($_POST["msisdn"]);
		$pid = test_input($_POST["pid"]);
		$keyword = test_input($_POST["keyword"]);
	}
	if(!empty($_POST["submit"]))
	{
		$param = array('msisdn' => $msisdn,'pid' => $pid, 'keyword' => $keyword);
		$result1=$client->call('callback', $param);
	}
	
	//print_r($result1); 
	function test_input($data) 
  	{
	    $data = trim($data);
	    $data = stripslashes($data);
	    $data = htmlspecialchars($data);
	    return $data;
  	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Subscriber Report</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<!-- for search -->
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-2">
		    		<h2>Callback Report</h2>
		        </div>
		</div><br/>
	    <div class="row">
	        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>">
	        	<div class="col-md-2 col-md-offset-2">
					<div class="form-group">
						<input type="text" name="msisdn" class="form-control" placeholder="Msisdn">
					</div>
	        	</div>
	        	<div class="col-md-2">
					<div class="form-group">
						<input type="text" name="pid" class="form-control" placeholder="Plan ID">
					</div>
	        	</div>
	        	<div class="col-md-2">
					<div class="form-group">
						<input type="text" name="keyword" class="form-control" placeholder="Keyword">
					</div>
	        	</div>
	        	<!--<div class="col-md-2">
					<div class="form-group">
						<select name="stype" class="form-control">
							<option value="" disabled="disabled" selected>Service Type</option>
							<option value="call">CALL</option>
							<option value="sms">SMS</option>
							<option value="voice sms">VOICE SMS</option>
						</select>
					</div>
	        	</div>-->
	        	<div class="col-md-1">
					<input type="submit" name="submit" class="btn btn-primary" value="Search">
	        	</div>
	        </form>
	    </div>
	</div>
	<div class="container">
	<?php
		if(empty($msisdn) && empty($sid) && empty($pid))
		{
			echo "
			<div class='well well-sm'>
				<h3 style='color:blue;'>Type To Search!</h3>
	      			<span style='color:blue'><i>Hints:</i></span><br/>
	      			<ul>
						<li><em>For better result Msisdn,Plan ID,Service Type should be accurate.</em></li>
					</ul> 
	      		</div>";
		}
		else
		{?>
			<div class='panel panel-default'>
			<div class='panel-body'>
				<table class='table table-striped' style='word-wrap:break-word;'>
			    <thead>
				    <tr>
				       	<th>ID</th>
				       	<th>Msisdn</th>
				       	<th>Service ID</th>
				       	<th>Service Type</th>
				       	<th>Plan ID</td>
				       	<th>Error Code</th>
				       	<th>Operation</th>
				       	<th>Result</th>
				       	<th>Trans ID</th>
				       	<th>Content ID</th>
				       	<th>Category</th>
				       	<th>Charge Amount</th>
				       	<th>Applied Plan</th>
				       	<th>End Date</th>
				       	<th>Validity Days</th>
				    </tr>
			    </thead>
				<tbody>
					<tr>
						<td><?php echo $result1['uid']; ?></td>
						<td><?php echo $result1['msdn']; ?></td>
						<td><?php echo $result1['srvid']; ?></td>
						<td><?php echo $result1['stype']; ?></td>
						<td><?php echo $result1['plnid']; ?></td>
						<td><?php echo $result1['errcod']; ?></td>
						<td><?php echo $result1['operation']; ?></td>
						<td><?php echo $result1['result']; ?></td>
						<td><?php echo $result1['transid']; ?></td>
						<td><?php echo $result1['contid']; ?></td>
						<td><?php echo $result1['catagry']; ?></td>
						<td><?php echo $result1['chrgamont']; ?></td>
						<td><?php echo $result1['applidpln']; ?></td>
						<td><?php echo $result1['enddts']; ?></td>
						<td><?php echo $result1['valdate']; ?></td>
					</tr>
				</tbody>
			<?php
			}
		?>
			</table>
	</div>
</body>
</html>