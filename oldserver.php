<?php
	require_once 'lib/nusoap.php';
	//error_reporting(0);

	//configuring wsdl.....
 	$server = new nusoap_server;
	$server->configureWSDL('server', 'urn:server');
	$server->wsdl->schemaTargetNamespace = 'urn:server';
	$server->wsdl->addComplexType(
	    'Person',
	    'complexType',
	    'struct',
	    'all',
	    '',
	    array(
	        'uid' => array('name' => 'uid', 'type' => 'xsd:int'),
	        'servicename' => array('name' => 'servicename', 'type' => 'xsd:string'),
	        'keywords' => array('name' => 'keywords', 'type' => 'xsd:string'),
	        'baseprice' => array('name' => 'baseprice', 'type' => 'xsd:string'),
	        'shortcodes' => array('name' => 'shortcodes', 'type' => 'xsd:string'),
	        'servicenode' => array('name' => 'servicenode', 'type' => 'xsd:string'),
	        'planid' => array('name' => 'planid', 'type' => 'xsd:string'),
	        'servicetype' => array('name' => 'servicetype', 'type' => 'xsd:string'),
	        'activedays' => array('name' => 'activedays', 'type' => 'xsd:string'),
	        'serviceid' => array('name' => 'serviceid', 'type' => 'xsd:string')
	    )
	);
	$server->wsdl->addComplexType(
	    'Subs',
	    'complexType',
	    'struct',
	    'all',
	    '',
	    array(
	    	'uid' => array('name' => 'uid', 'type' => 'xsd:int'),
	    	'msisdns' => array('name' => 'msisdns', 'type' => 'xsd:string'),
	    	'planid' => array('name' => 'planid', 'type' => 'xsd:string'),
	    	'keywords' => array('name' => 'keywords', 'type' => 'xsd:string'),
	    	'shortcodes' => array('name' => 'shortcodes', 'type' => 'xsd:string'),
	        'initialstatus' => array('name' => 'initialstatus', 'type' => 'xsd:string'),
	        'previousstatus' => array('name' => 'previousstatus', 'type' => 'xsd:string'),
	        'currentstatus' => array('name' => 'currentstatus', 'type' => 'xsd:string'),
	        'servicetype' => array('name' => 'servicetype', 'type' => 'xsd:string')
	    )
	);
	//registration for sevice list
	$server->register('serviceList',
		array('keyword'=>'xsd:string', 'planid'=>'xsd:string', 'serviceid'=>'xsd:string'),
		array('return' => 'tns:Person'),  //output
		'urn:server',   //namespace
		'urn:server#servicelistServer',  //soapaction
		'rpc', // style
		'encoded', // use
		'Just say hello');  //description

	$server->register('subscriber',
		array('msisdn'=>'xsd:string', 'plan_id'=>'xsd:string', 'keyword'=>'xsd:string'),
		array('return' => 'tns:Subs'),  //output
		'urn:server',   //namespace
		'urn:server#subscriberlistServer',  //soapaction
		'rpc', // style
		'encoded', // use
		'subscriber');  //description
	function serviceList($keyword,$pid,$sid)
	{
		include 'dbcon.php';
		$sql = "SELECT * FROM service_list where keyword like '%$keyword%' or plan_id like '%$pid%' or service_id like '%$sid%'";
		$result = $conn->query($sql);
		$row = $result->fetch_assoc();

		return array(
			'uid' => $row['id'],
		    'servicename' => $row['service_name'],
		    'keywords' => $row['keyword'],
	        'baseprice' => $row['base_price'],
		    'shortcodes' => $row['shortcode'],
		    'servicenode' => $row['service_node'],
		    'planid' => $row['plan_id'],
		    'servicetype' => $row['service_type'],
		    'activedays' => $row['active_days'],
		    'serviceid' => $row['service_id']
		);
	}
	function subscriber($msisdn,$pid,$keyword)
	{
		include 'dbcon.php';
		$sql = "SELECT * FROM subscribers where msisdn like '%$msisdn%' or plan_id like '%$pid%' or keyword like '%$keyword%' ";
		$result = $conn->query($sql);
		$row = $result->fetch_assoc();
		return array(
			'uid' => $row['id'],
		    'msisdns' => $row['msisdn'],
		    'planid' => $row['plan_id'],
	        'keywords' => $row['keyword'],
		    'shortcodes' => $row['shortcode'],
		    'initialstatus' => $row['initial_status'],
		    'previousstatus' => $row['previous_status'],
		    'currentstatus' => $row['current_status'],
		    'servicetype' => $row['service_type']
		);
	}

	$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
	$server->service($HTTP_RAW_POST_DATA);
	/*
	$idn = $row['id'];
		$sname = $row['service_name'];
		$kword = $row['keyword'];
		$bsprc = $row['base_price'];
		$shcd = $row['shortcode'];
		$snod = $row['service_node'];
		$pids = $row['plan_id'];
		$styp = $row['service_type'];
		$actdy = $row['active_days'];
		$sids = $row['service_id'];
		return array(
			'uid' => $idn,
		    'servicename' => $sname,
		    'keywords' => $kword,
	        'baseprice' => $bsprc,
		    'shortcodes' => $shcd,
		    'servicenode' => $snod,
		    'planid' => $pids,
		    'servicetype' => $styp,
		    'activedays' => $actdy,
		    'serviceid' => $sids
		);*?
?>